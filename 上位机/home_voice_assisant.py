# -*- coding: utf-8 -*-
"""
【N32G457】基于RT-Thread和N32G457的家庭语音助手
应用方案说明及使用场景描述：
本方案通过N32G457+RT-Thread，实现一个家庭语音助手，提供语音唤醒、多路开关、倒计时、闹钟提醒、天气提醒、新闻播报、音乐播放等功能，为家庭生活增添便利和趣味性。
具体实现功能如下：
1.一键配网(未实现)
2.特定唤醒词唤醒
3.语音控制多路开关
4.语音设置倒计时
5.语音设置闹钟(未实现)
6.语音播报天气提醒
7.语音播报新闻
8.语音控制音乐播放
"""
from os import path
import random
import wave
import const
import websocket

import threading
import time
import uuid
import json
import logging
import sys

import pyaudio
import serial
import json


from urllib.request import urlopen
from urllib.request import Request
from urllib.error import URLError
from urllib.parse import urlencode
from urllib.parse import quote_plus

logger = logging.getLogger()

CHUNK = 1600  # 每个缓冲区的帧数
FORMAT = pyaudio.paInt16  # 采样位数
CHANNELS = 1  # 单声道
RATE = 16000  # 采样频率


"""
百度tts
"""
# 发音人选择, 基础音库：0为度小美，1为度小宇，3为度逍遥，4为度丫丫，
# 精品音库：5为度小娇，103为度米朵，106为度博文，110为度小童，111为度小萌，默认为度小美 
PER = 0
# 语速，取值0-15，默认为5中语速
SPD = 5
# 音调，取值0-15，默认为5中语调
PIT = 5
# 音量，取值0-9，默认为5中音量
VOL = 5
# 下载的文件格式, 3：mp3(default) 4： pcm-16k 5： pcm-8k 6. wav
AUE = 6
TTS_FORMATS = {3: "mp3", 4: "pcm", 5: "pcm", 6: "wav"}
TTS_FORMAT = TTS_FORMATS[AUE]

class DemoError(Exception):
    pass


"""  TOKEN start """
SCOPE = 'audio_tts_post'  # 有此scope表示有tts能力，没有请在网页里勾选

def fetch_token():
    print("fetch token begin")
    params = {'grant_type': 'client_credentials',
              'client_id': const.APPKEY,
              'client_secret': const.SECRET_KEY}
    post_data = urlencode(params)
    post_data = post_data.encode('utf-8')
    req = Request(const.TOKEN_URL, post_data)
    try:
        f = urlopen(req, timeout=5)
        result_str = f.read()
    except URLError as err:
        print('token http response http code : ' + str(err.code))
        result_str = err.read()
    result_str = result_str.decode()

    print(result_str)
    result = json.loads(result_str)
    print(result)
    if ('access_token' in result.keys() and 'scope' in result.keys()):
        if not SCOPE in result['scope'].split(' '):
            raise DemoError('scope is not correct')
        print('SUCCESS WITH TOKEN: %s ; EXPIRES IN SECONDS: %s' % (result['access_token'], result['expires_in']))
        return result['access_token']
    else:
        raise DemoError('MAYBE API_KEY or SECRET_KEY not correct: access_token or scope not found in token response')
"""  TOKEN end """

def tts(Text):
    #如果文件已缓存则直接返回文件名
    file_name = Text+'-' + str(PER) + '.' + TTS_FORMAT
    if path.isfile(file_name):
        logger.info(file_name+" exist")
        return file_name
    logger.info("TTS:"+Text)
    tex = quote_plus(Text)  # 此处TEXT需要两次urlencode
    print(tex)
    params = {'tok': token, 'tex': tex, 'per': PER, 'spd': SPD, 'pit': PIT, 'vol': VOL, 'aue': AUE, 'cuid': const.CUID,
              'lan': 'zh', 'ctp': 1}  # lan ctp 固定参数

    data = urlencode(params)
    print('test on Web Browser' + const.TTS_URL + '?' + data)

    req = Request(const.TTS_URL, data.encode('utf-8'))
    has_error = False
    try:
        f = urlopen(req)
        result_str = f.read()

        headers = dict((name.lower(), value) for name, value in f.headers.items())

        has_error = ('content-type' not in headers.keys() or headers['content-type'].find('audio/') < 0)
    except  URLError as err:
        print('asr http response http code : ' + str(err.code))
        result_str = err.read()
        has_error = True

    save_file = "error.txt" if has_error else Text+'-' + str(PER) + '.' + TTS_FORMAT
    with open(save_file, 'wb') as of:
        of.write(result_str)

    if has_error:
        result_str = str(result_str, 'utf-8')
        print("tts api  error:" + result_str)

    print("result saved as :" + save_file)
    return save_file

"""
实时流式识别
需要安装websocket-client库

1. 连接 ws_app.run_forever()
2. 连接成功后发送数据 on_open()
2.1 发送开始参数帧 send_start_params()
2.2 发送音频数据帧 send_audio()
2.3 库接收识别结果 on_message()
2.4 发送结束帧 send_finish()
3. 关闭连接 on_close()

库的报错 on_error()
"""

def send_start_params(ws):
    """
    开始参数帧
    :param websocket.WebSocket ws:
    :return:
    """
    req = {
        "type": "START",
        "data": {
            "appid": const.APPID,  # 网页上的appid
            "appkey": const.APPKEY,  # 网页上的appid对应的appkey
            "dev_pid": const.DEV_PID,  # 识别模型
            "cuid": const.CUID,  # 随便填不影响使用。机器的mac或者其它唯一id，百度计算UV用。
            "sample": 16000,  # 固定参数
            "format": "pcm"  # 固定参数
        }
    }
    body = json.dumps(req)
    ws.send(body, websocket.ABNF.OPCODE_TEXT)
    logger.info("send START frame with params:" + body)


def send_audio(ws):
    """
    发送二进制音频数据，注意每个帧之间需要有间隔时间
    :param  websocket.WebSocket ws:
    :return:
    """
    pcm_file = "16k-0.pcm"
    chunk_ms = 160  # 160ms的录音
    chunk_len = int(16000 * 2 / 1000 * chunk_ms)
    with open(pcm_file, 'rb') as f:
        pcm = f.read()

    index = 0
    total = len(pcm)
    logger.info("send_audio total={}".format(total))
    while index < total:
        end = index + chunk_len
        if end >= total:
            # 最后一个音频数据帧
            end = total
        body = pcm[index:end]
        logger.debug("try to send audio length {}, from bytes [{},{})".format(len(body), index, end))
        ws.send(body, websocket.ABNF.OPCODE_BINARY)
        index = end
        time.sleep(chunk_ms / 1000.0)  # ws.send 也有点耗时，这里没有计算

def send_pyaudio(ws):
    """
    pyaudio实时读取并发送音频流
    CHUNK大小为160ms语音长度
    16000*16/160=1600
    """
    while True:
        data = stream.read(CHUNK)
        ws.send(data, websocket.ABNF.OPCODE_BINARY)

def send_finish(ws):
    """
    发送结束帧
    :param websocket.WebSocket ws:
    :return:
    """
    req = {
        "type": "FINISH"
    }
    body = json.dumps(req)
    ws.send(body, websocket.ABNF.OPCODE_TEXT)
    logger.info("send FINISH frame")


def send_cancel(ws):
    """
    发送取消帧
    :param websocket.WebSocket ws:
    :return:
    """
    req = {
        "type": "CANCEL"
    }
    body = json.dumps(req)
    ws.send(body, websocket.ABNF.OPCODE_TEXT)
    logger.info("send Cancel frame")


def on_open(ws):
    """
    连接后发送数据帧
    :param  websocket.WebSocket ws:
    :return:
    """
    def run(*args):
        """
        发送数据帧
        :param args:
        :return:
        """
        send_start_params(ws)
        send_pyaudio(ws)
        send_finish(ws)
        logger.debug("thread terminating")

    threading.Thread(target=run).start()

def on_message(ws, message):
    """
    接收服务端返回的消息
    :param ws:
    :param message: json格式，自行解析
    :return:
    """
    #logger.info("Response: " + message)
    result = json.loads(message)
    if result.get("result", "") != "":
        logger.info("识别结果:"+result["result"])
        if result["result"][-1] in ["。", "？"]:
            process_instruct(result["result"])

def process_instruct(instruct):
    """
    处理语音指令
    """
    instruct = str(instruct).replace("。", "").replace("？", "")
    cmd = ""
    voice = ""
    match instruct:
        case "小白小白":
            cmd = "mini_rgb up"
            voices = "在呢|嗯|爷爷在此".split("|")
            random.shuffle(voices)
            voice = voices[0]
        case "打开红灯":
            cmd = "light_control red on"
            voice = "红灯已打开"
        case "关闭红灯":
            cmd = "light_control red off"
            voice = "红灯已关闭"
        case "打开绿灯":
            cmd = "light_control green on"
            voice = "绿灯已打开"
        case "关闭绿灯":
            cmd = "light_control green off"
            voice = "绿灯已关闭"
        case "打开蓝灯":
            cmd = "light_control blue on"
            voice = "蓝灯已打开"
        case "关闭蓝灯":
            cmd = "light_control blue off"
            voice = "蓝灯已关闭"
        case "天气":
            cmd = "mini_rgb up"
            voice = "有你的每一天都是晴天"
        case "几点了":
            cmd = "mini_rgb up"
            now = time.strftime("%Y年%m月%d日%H点%M分%S秒", time.localtime()) 
            voice = "现在是" + now
        case "早上好":
            cmd = "mini_rgb up"
            voice = "morning"
        case "我想听歌":
            cmd = "mini_rgb up"
            voice = "一闪一闪亮晶晶，满天都是小星星"
        case "倒计时五秒钟":
            cmd = "count_down 5"
            voice = "已为您设置好倒计时"
        case "新闻":
            cmd = "mini_rgb up"
            voice = "【一眼新闻丨3月16日贝果财经早间资讯】中国塞尔维亚驾照互认正式生效；106班上海入境国际航班将更改入境点；邓伦偷逃税被处罚并追缴1.06亿元；最高法明确电商不得以拆封为由拒绝退货；茅台回应潘长江直播事件；白俄罗斯和俄罗斯将在能源交易中放弃使用美元；英国将取消所有新冠疫情旅行限制措施；韩国开发全球首个可拉伸无失真元显示技术；美国航天局证实：美航天员将乘俄飞船返回......"
        case "再见":
            cmd = "mini_rgb down"
            voice = "再见"
        case _:
            cmd = "mini_rgb down"
            voices = "不知道你说了什么|不懂".split("|")
            random.shuffle(voices)
            voice = voices[0]
    if cmd != "":
        logger.info('cmd:'+cmd)
        cmd = cmd + "\n"
        cmd = cmd.encode("ascii")
        n32_serial.write(cmd)
    if voice != "":
        tts_file = tts(voice)
        if tts_file != "":
            play_audio(tts_file)

def play_audio(file):
    """
    播放声音
    """
    p_play = pyaudio.PyAudio()  # 实例化
    wf = wave.open(file, 'rb')  # 读 wav 文件
    stream_play = p_play.open(format=p_play.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True)
    data = wf.readframes(CHUNK)  # 读数据
    while len(data) > 0:
        stream_play.write(data)
        data = wf.readframes(CHUNK)

    stream_play.stop_stream()  # 关闭资源
    stream_play.close()
    p_play.terminate()

def on_error(ws, error):
    """
    库的报错，比如连接超时
    :param ws:
    :param error: json格式，自行解析
    :return:
    """
    logger.error("error: " + str(error))


def on_close(ws, close_status_code, close_status_msg):
    """
    Websocket关闭
    :param websocket.WebSocket ws:
    :return:
    """
    logger.info("ws close ... [" + str(close_status_code) + "]" + close_status_msg)
    # ws.close()

def serial_read(*args):
    """
    N32G457串口读取
    """
    while True:
        if n32_serial.in_waiting:
            data = n32_serial.readline().decode("utf-8", "ignore").strip()
            logger.info(data)
            if data == "count_down_achieved":
                voice = "倒计时时间到"
                tts_file = tts(voice)
                if tts_file != "":
                    play_audio(tts_file)
if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)-15s] [%(funcName)s()][%(levelname)s] %(message)s')
    logger.setLevel(logging.DEBUG)  # 调整为logging.INFO，日志会少一点
    logger.info("begin")

    #串口初始化
    serial_name = "COM3"
    n32_serial = serial.Serial(serial_name, 115200, timeout=3)
    if n32_serial.isOpen() == False:
        logger.info("serial open failure")
    threading.Thread(target=serial_read).start()
    
    #pyaudio初始化
    p = pyaudio.PyAudio()  # 实例化对象
    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)  # 打开流，传入响应参数
    
    #tts
    token = fetch_token()

    # websocket.enableTrace(True)
    uri = const.ASR_URI + "?sn=" + str(uuid.uuid1())
    logger.info("uri is "+ uri)
    ws_app = websocket.WebSocketApp(uri,
                                    on_open=on_open,  # 连接建立后的回调
                                    on_message=on_message,  # 接收消息的回调
                                    on_error=on_error,  # 库遇见错误的回调
                                    on_close=on_close)  # 关闭后的回调

    ws_app.run_forever()

    n32_serial.close()
    stream.stop_stream()  # 关闭流
    stream.close()
    p.terminate()

    logger.info("ending...............................")
