# -*- coding: utf-8 -*-
"""
常量
"""

# 下面是鉴权信息
APPID = 1 #替换为百度开发者账号APPID

APPKEY = "***"#替换为百度开发者账号APPKEY

SECRET_KEY = "***"#替换为百度开发者账号SECRET_KEY

CUID = "Home-Voice-Assistant"

# 语言模型 ， 可以修改为其它语言模型测试，如远场普通话19362
DEV_PID = 15372

# 可以改为wss://
ASR_URI = "ws://vop.baidu.com/realtime_asr"

TOKEN_URL = 'http://aip.baidubce.com/oauth/2.0/token'
TTS_URL = 'http://tsn.baidu.com/text2audio'
