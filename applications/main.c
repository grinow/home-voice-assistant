/*
 * 应用方案说明及使用场景描述：
 * 本方案通过N32G457+RT-Thread，实现一个家庭语音助手，提供语音唤醒、多路开关、倒计时、闹钟提醒、天气提醒、新闻播报、音乐播放等功能，为家庭生活增添便利和趣味性。
 * 具体实现功能如下：
 * 1.一键配网(未实现)
 * 2.特定唤醒词唤醒
 * 3.语音控制多路开关
 * 4.语音设置倒计时
 * 5.语音设置闹钟(未实现)
 * 6.语音播报天气提醒
 * 7.语音播报新闻
 * 8.语音控制音乐播放
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-21     Leon         my first hardware project
 */
#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include "n32g45x_gpio.h"

#define LED_GREEN_PIN       91 //100 PIN PB5 绿
#define LED_RED_PIN         67 //100 PIN PA8 红
#define LED_BLUE_PIN        90 //100 PIN PB4 蓝

#define ADC_DEV_NAME        "adc1"      /* ADC 设备名称 */
#define ADC_DEV_CHANNEL     6           /* ADC 通道 */
#define REFER_VOLTAGE       330         /* 参考电压 3.3V,数据精度乘以100保留2位小数*/
#define CONVERT_BITS        (1 << 12)   /* 转换位数为12位 */

rt_adc_device_t adc_dev;
rt_uint32_t value, vol;
rt_err_t ret = RT_EOK;

/* 定时器的控制块 */
static rt_timer_t timer1;
static rt_timer_t timer2;
static int cnt = 0;

rt_uint32_t adc_read()
{
    /* 读取采样值 */
    value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);
    rt_kprintf("the value is :%d \n", value);
    return value;
}

int adc_init()
{
    /* 查找设备 */
    adc_dev = (rt_adc_device_t)rt_device_find(ADC_DEV_NAME);
    if (adc_dev == RT_NULL)
    {
        rt_kprintf("adc sample run failed! can't find %s device!\n", ADC_DEV_NAME);
        return 0;
    }

    /* 使能设备 */
    ret = rt_adc_enable(adc_dev, ADC_DEV_CHANNEL);
    return ret;
}

void adc_disable()
{
    /* 关闭通道 */
    ret = rt_adc_disable(adc_dev, ADC_DEV_CHANNEL);
}

/*
 * 灯光控制
 */
static int light_control(int argc, char *argv[])
{
    if (argc != 3) {
        rt_kprintf("light control usage:light_control red|green|blue on|off\n");
        return 0;
    }
    int pin = LED_GREEN_PIN;
    int status = PIN_LOW;
    if (rt_strcmp(argv[1], "red") == 0) {
        pin = LED_RED_PIN;
    } else if (rt_strcmp(argv[1], "blue") == 0) {
        pin = LED_BLUE_PIN;
    } else {
        pin = LED_GREEN_PIN;
    }
    if (rt_strcmp(argv[2], "on") == 0) {
        status = PIN_HIGH;
    } else {
        status = PIN_LOW;
    }
    rt_pin_write(pin, status);
}

/*
 * mini RGB ^^
 */
static int mini_rgb(int argc, char *argv[])
{
    if (argc != 2) {
        rt_kprintf("usage:rgb up|down\n");
        return 0;
    }
    int leds[3];
    int start = 0;
    int end = 3;
    int loop = 3;
    int delay = 100;
    if (rt_strcmp(argv[1], "up") == 0) {
        leds[0] = LED_RED_PIN;
        leds[1] = LED_BLUE_PIN;
        leds[2] = LED_GREEN_PIN;
    } else {
        leds[0] = LED_GREEN_PIN;
        leds[1] = LED_BLUE_PIN;
        leds[2] = LED_RED_PIN;
    }

    for (int i = 0; i < loop; i++) {
        for (int j = 0; j < 3; j++) {
            int prev_index = j > 0 ? j - 1 : 2;
            rt_pin_write(leds[j], PIN_HIGH);
            rt_pin_write(leds[prev_index], PIN_LOW);
            rt_thread_mdelay(delay);
        }
        rt_pin_write(leds[2], PIN_LOW);
        rt_thread_mdelay(delay);
    }
}

/* 定时器 2 倒计时语音提醒
 * 采样频率8000
 */
static void count_down_achieved(void *parameter)
{
    rt_kprintf("\n");
    rt_kprintf("count_down_achieved\n");
    char *arg[2] = {"", "up"};
    mini_rgb(2, arg);
}

/*
 * count down
 */
static int count_down(int argc, char *argv[])
{
    if (argc != 2) {
        rt_kprintf("usage:count_down xx(s)\n");
        return 0;
    }
    int ticks = atoi(argv[1]) * 100;
    timer2 = rt_timer_create("timer2", count_down_achieved,
                                 RT_NULL, ticks,
                                 RT_TIMER_FLAG_ONE_SHOT);

    /* 启动定时器  */
    if (timer2 != RT_NULL) rt_timer_start(timer2);
}
/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(light_control, light_control red|green on|off);
MSH_CMD_EXPORT(mini_rgb, mini rgb);
MSH_CMD_EXPORT(count_down, count down);

/* 定时器 1 音频采样函数（未完成）
 * 采样频率8000
 */
static void audio_sample(void *parameter)
{
    // todo:实现微秒定时器，每125us采样一次
    /*cnt++;
    //rt_kprintf("periodic timer is timeout %d\n", cnt);
    int i = 0;
    while (1)
    {
        //value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);
        i++;
        if (i >= 8000) {
            //rt_kprintf("convert %d, %x\n", value, 1);
            //rt_kprintf("reading %d, %d\n", cnt, i);
            break;
        }
    }*/
    /* 运行第 10 次，停止周期定时器 */
    /*if (cnt++>= 10)
    {
        rt_timer_stop(timer1);
        rt_kprintf("periodic timer was stopped! \n");
    }*/
}


int main(void)
{
    //禁用JTAG功能
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO,ENABLE);
    GPIO_ConfigPinRemap(GPIO_RMP_SW_JTAG_SW_ENABLE, ENABLE);

    rt_pin_mode(LED_GREEN_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED_RED_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED_BLUE_PIN, PIN_MODE_OUTPUT);
    rt_pin_write(LED_BLUE_PIN, PIN_LOW);

    rt_kprintf("boot at %d\n", rt_tick_get());
    adc_init();
    rt_kprintf("adc_init at %d\n", rt_tick_get());
    for (int i = 0; i < 8000; i++)
    {
        value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);
        //rt_kprintf("rt_adc_read at %04x\n", value);
    }
    rt_kprintf("rt_adc_read at %d\n", rt_tick_get());
    /* 创建定时器 1  周期定时器
     * 每160ms创建一个声音片段
     */
    timer1 = rt_timer_create("timer1", audio_sample,
                             RT_NULL, 16,
                             RT_TIMER_FLAG_PERIODIC);

    /* 启动定时器 1 */
    if (timer1 != RT_NULL) rt_timer_start(timer1);

}

