################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/libcpu/arm/cortex-m4/cpuport.c 

S_UPPER_SRCS += \
../rt-thread/libcpu/arm/cortex-m4/context_gcc.S 

OBJS += \
./rt-thread/libcpu/arm/cortex-m4/context_gcc.o \
./rt-thread/libcpu/arm/cortex-m4/cpuport.o 

S_UPPER_DEPS += \
./rt-thread/libcpu/arm/cortex-m4/context_gcc.d 

C_DEPS += \
./rt-thread/libcpu/arm/cortex-m4/cpuport.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/libcpu/arm/cortex-m4/%.o: ../rt-thread/libcpu/arm/cortex-m4/%.S
	arm-none-eabi-gcc -x assembler-with-cpp -Xassembler -mimplicit-it=thumb -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -x assembler-with-cpp -Wa,-mimplicit-it=thumb  -gdwarf-2 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
rt-thread/libcpu/arm/cortex-m4/%.o: ../rt-thread/libcpu/arm/cortex-m4/%.c
	arm-none-eabi-gcc -DARM_MATH_CM4 -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\Libraries\N32_Std_Driver\CMSIS\core" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\Libraries\N32_Std_Driver\CMSIS\device" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\Libraries\N32_Std_Driver\n32g45x_std_periph_driver\inc" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\Libraries\rt_drivers" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\applications" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\board\msp" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\board" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\components\libc\compilers\gcc\newlib" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\include" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\libcpu\arm\common" -I"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rt-thread\libcpu\arm\cortex-m4" -include"D:\RT-ThreadStudio\workspace\Home-Voice-Assistant\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

